#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: caupetit <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/01/05 15:46:59 by caupetit          #+#    #+#              #
#    Updated: 2015/06/03 18:12:02 by caupetit         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = scop
SRC = main.c shader.c error.c mat4.c mat4_transform.c operators_mat4.c \
	vec3.c vec3_sub.c vec3_mult.c vec3_add.c \
	cam.c callback.c obj_pars.c load_obj.c load_texture.c compute_normals.c \
	obj_list.c display.c skybox.c update.c
CC = clang
FLAGS = -Wall -Wextra -Werror -g
OBJ = $(SRC:%.c=$(ODIR)/%.o)
HEADERS = 
SRCDIR = srcs
IDIR = includes
ODIR = objs
LIBFT= -L./libft/ -lft
DIRGLFW=glfw
GLFWINCLUDES = $(DIRGLFW)/include
GLFWFRAMEWORK = -L./$(DIRGLFW)/src/ -lglfw3 -framework Cocoa -framework OpenGL \
				-framework IOKit -framework CoreVideo
#GLFWFRAMEWORK = -L./$(DIRGLFW)/src/ -lGL -lGLU -lglfw3 -lX11 -lXxf86vm -lXrandr -lpthread -lXi -lm -lXcursor -lXinerama
all: glfw/src glfw/src/libglfw3.a libft/libft.a $(ODIR) $(NAME)

$(ODIR):
	@mkdir -p $(ODIR)
	@echo "\033[32m->\033[0m Directory \033[36m$(ODIR)\033[0m created"

libft/libft.a:
	@echo -n libft:
	@make -C libft/
	@echo ""

glfw/src:
	@git submodule init
	@git submodule update

glfw/src/libglfw3.a:
	cd glfw && cmake GLFW_BUILD_EXAMPLES=false GLFW_BUILD_TESTS=false \
					GLFW_BUILD_DOCS=false .  && cd src/ && make

$(NAME): $(OBJ) $(IDIR)
	@echo "\033[32m=>\033[0m Compiling \033[33m$(NAME)\033[0m"
	$(CC) $(FLAGS) -o $@ $(OBJ) -I$(IDIR) -I $(GLFWINCLUDES) -I libft/includes $(GLFWFRAMEWORK) $(LIBFT)
	@echo ""
	@echo "Compilation \033[32mSuccess\033[0m"

$(ODIR)/%.o: $(SRCDIR)/%.c $(IDIR)
	@echo "\033[32m->\033[0m Compiling \033[33m$<\033[0m"
	$(CC) $(FLAGS) -o $@ -c $< -I $(IDIR) -I $(GLFWINCLUDES) -I libft/includes
	@echo ""

clean:
	@rm -f $(OBJ)
	@echo "\033[31m->\033[0m Deleted \033[33m.o\033[0m"

clear_libs:
	@make -C libft/ fclean
	@make -C glfw/ clean

libs:
	@make libft/libft.a
	@make -C glfw/src/

fclean: clear clean
	@rm -rf $(ODIR)
	@echo "\033[31m->\033[0m Directory \033[36m$(ODIR)\033[0m deleted"
	@rm -f $(NAME)
	@echo "\033[31m->\033[0m Deleted \033[33m$(NAME)\033[0m"

ffclean: fclean clear_libs

re: fclean all

clear:
	@rm -f *~
	@rm -f **/*~
	@echo "\033[31m->\033[0m cleared ~ files"

list: clear
	@ls -G1 `find . -name .git -prune -o -print`
#	@ls -RA1 --color -I .git       list for linux.

.PHONY: glfw
