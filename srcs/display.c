/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/01 19:10:55 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/04 18:35:46 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define GLFW_INCLUDE_GLCOREARB

#include "scop.h"
#include "mat4.h"

static void		get_model_matrix(t_env *env, t_3do *obj,
								GLfloat *model, const t_vec3 pos)
{
	GLfloat	trans[16];
	GLfloat	scale[16];
	GLfloat	rot[16];
	t_vec3	world_pos;

	world_pos = subv3(pos, env->scene_center);
	rot_mat4(rot, glfwGetTime() * 100 / 4, vec3(0, 1, 0));
	trans_mat4(trans, vec3(-obj->trans[0], -obj->trans[1], -obj->trans[2]));
	mult_mat4(rot, rot, trans);
	trans_mat4(trans, world_pos);
	mult_mat4(model, trans, rot);
	scale_mat4(scale, vec3(obj->scale, obj->scale, obj->scale));
	mult_mat4(model, model, scale);
	rot_mat4(rot, glfwGetTime() * 2, vec3(1, 0, 0));
}

static void		set_mix_value(t_env *env, t_3do *obj)
{
	static double	mix = 0;
	static double	speed = 1 / 360.0;
	GLuint			ids[2];

	if (env->display_mode > mix)
		mix = mix + speed > 1.0 ? 1.0 : mix + speed;
	else if (env->display_mode < mix)
		mix = mix - speed < 0.0 ? 0.0 : mix - speed;
	if (env->display_mode > 1)
		mix = 0.5;
	ids[0] = glGetUniformLocation(obj->shader, "mix_value");
	glUniform1f(ids[0], mix);
	ids[1] = glGetUniformLocation(obj->shader, "mode");
	glUniform1f(ids[1], env->display_mode);
}

static void		bind_obj_texture(t_3do *obj)
{
	GLuint		id;

	glBindTexture(GL_TEXTURE_2D, obj->texture_id);
	id = glGetUniformLocation(obj->shader, "tm");
	glUniform1i(id, 0);
}

void			draw_obj(t_env *env, t_3do *obj, const t_vec3 pos)
{
	GLfloat	proj[16];
	GLfloat	view[16];
	GLfloat	model[16];
	GLuint	ids[3];

	proj_mat4(proj, 0.8, 5000, 0.1);
	viewfps_mat4(view, env->cam.origin, env->cam.pitch, env->cam.yaw);
	get_model_matrix(env, obj, model, pos);
	glBindVertexArray(obj->vao);
	glUseProgram(obj->shader);
	ids[0] = glGetUniformLocation(obj->shader, "view");
	glUniformMatrix4fv(ids[0], 1, GL_FALSE, view);
	ids[1] = glGetUniformLocation(obj->shader, "proj");
	glUniformMatrix4fv(ids[1], 1, GL_FALSE, proj);
	ids[2] = glGetUniformLocation(obj->shader, "model");
	glUniformMatrix4fv(ids[2], 1, GL_FALSE, model);
	bind_obj_texture(obj);
	set_mix_value(env, obj);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glDrawElements(GL_TRIANGLES, obj->inb * 3,
				GL_UNSIGNED_INT, NULL);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
}

void			display_objs(t_env *env, t_3do_l **begin)
{
	t_3do_l		*tmp;
	double		step;
	double		angle;
	int			rad;
	int			obj_nb;

	tmp = *begin;
	obj_nb = 0;
	while (tmp && tmp->next)
	{
		obj_nb++;
		tmp = tmp->next;
	}
	rad = obj_nb * 4.2;
	step = M_PI * 2.0 / obj_nb;
	angle = 0.1 * glfwGetTime();
	tmp = *begin;
	draw_obj(env, tmp->elem, vec3(0, 0, 0));
	tmp = tmp->next;
	while (tmp)
	{
		draw_obj(env, tmp->elem, vec3(cos(angle) * rad, sin(angle) * rad, 0));
		angle += step;
		tmp = tmp->next;
	}
}
