/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 14:27:41 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/21 16:20:23 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "vec3.h"

t_vec3		vec3(float x, float y, float z)
{
	t_vec3	ret;

	ret.x = x;
	ret.y = y;
	ret.z = z;
	return (ret);
}

t_vec3		normalisev3(t_vec3 v)
{
	double	magn;

	magn = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	return (vec3(v.x / magn, v.y / magn, v.z / magn));
}

float		dotv3(t_vec3 a, t_vec3 b)
{
	return (a.x * b.x + a.y * b.y + a.z * b.z);
}

t_vec3		crossv3(t_vec3 a, t_vec3 b)
{
	return (vec3(a.y * b.z - a.z * b.y,
	a.z * b.x - a.x * b.z,
	a.x * b.y - a.y * b.x));
}
