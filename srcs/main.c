/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 14:52:07 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/05 15:44:20 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define GLFW_INCLUDE_GLCOREARB

#include <stdio.h>
#include <stdlib.h>
#include <GLFW/glfw3.h>
#include <string.h>
#include "scop.h"
#include "load_texture.h"
#include "error.h"

GLFWwindow		*init_window(void)
{
	GLFWwindow	*window;

	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		return (NULL);
	glfwWindowHint(GLFW_SAMPLES, 32);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(WIDTH, HEIGHT, "Scop", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return (NULL);
	}
	glfwMakeContextCurrent(window);
	printf("\nOpengl Version: %s\n\n", glGetString(GL_VERSION));
	glfwSwapInterval(1);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDisable(GL_BLEND);
	glClearColor(0, 0, 0, 0);
	glDisable(GL_POLYGON_SMOOTH);
	return (window);
}

int				init_program(GLFWwindow **window, t_env *env, t_3do_l **obj_l)
{
	if (!(*window = init_window()))
		return (error("Window initialisation"));
	glfwSetWindowUserPointer(*window, env);
	glfwSetCursorPosCallback(*window, cursor_pos_callback);
	bzero(env, sizeof(*env));
	init_cam(&env->cam);
	env->display_mode = 1;
	*obj_l = NULL;
	if (!get_3do_in_dir("assets/objs/", obj_l))
		return (error("Unable to load 3d objects"));
	if (!(env->tid[1] = load_texture("assets/demon_cat.bmp")))
		return (error("Unable to load texture"));
	if (!(env->tid[3] = load_texture("assets/hcat.bmp")))
		return (error("Unable to load texture"));
	if (!(env->tid[2] = load_texture("assets/cat128.bmp")))
		return (error("Unable to load texture"));
	if (!(env->tid[0] = load_texture("assets/cat.bmp")))
		return (error("Unable to load texture"));
	set_objs_textures(env, obj_l);
	return (true);
}

void			put_usage(void)
{
	printf("\nUsage:\n - \033[32mA D S W\033[0m to move the camera\n");
	printf(" - \033[32mMouse\033[0m ");
	printf("or \033[32mUP DOWN LEFT RIGHT\033[0m to look around\n");
	printf(" - \033[32mSpace\033[0m to stop mouse tracking\n");
	printf(" - \033[32m1 2 3\033[0m to change display mode\n");
	printf(" - \033[32mKeypad 4 5 6 7 8 9\033[0m to move object circle\n");
	printf(" - Add or remove .obj from \033[33massets/objs\033[0m folder\n");
}

int				main(void)
{
	GLFWwindow	*window;
	t_env		env;
	t_3do_l		*obj_list;
	t_skybox	sky;

	if (!init_program(&window, &env, &obj_list))
		return (EXIT_FAILURE);
	if (!load_skybox(&sky))
		return (EXIT_FAILURE);
	put_usage();
	while (!glfwWindowShouldClose(window))
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glfwPollEvents();
		handle_keys(window, &env);
		update_inputs(&env);
		display_sky(&env, &sky);
		display_objs(&env, &obj_list);
		glfwSwapBuffers(window);
	}
	obj_list_del(&obj_list);
	glfwDestroyWindow(window);
	glfwTerminate();
	return (EXIT_SUCCESS);
}
