/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat4_transform.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 13:55:22 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/13 20:09:53 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <strings.h>
#include "mat4.h"

void		trans_mat4(float *mat, t_vec3 t)
{
	ident_mat4(mat);
	mat[12] = t.x;
	mat[13] = t.y;
	mat[14] = t.z;
}

void		scale_mat4(float *mat, t_vec3 v)
{
	ident_mat4(mat);
	mat[0] = v.x;
	mat[5] = v.y;
	mat[10] = v.z;
}

void		rot_mat4(float *mat, float angle, t_vec3 u)
{
	float	c;
	float	s;

	angle = angle / 180 * M_PI;
	c = cos(angle);
	s = sin(angle);
	ident_mat4(mat);
	u = normalisev3(u);
	mat[0] = c + u.x * u.x * (1 - c);
	mat[1] = u.x * u.y * (1 - c) - u.z * s;
	mat[2] = u.x * u.z * (1 - c) + u.y * s;
	mat[4] = u.y * u.x * (1 - c) + u.z * s;
	mat[5] = c + u.y * u.y * (1 - c);
	mat[6] = u.y * u.z * (1 - c) - u.x * s;
	mat[8] = u.z * u.x * (1 - c) - u.y * s;
	mat[9] = u.z * u.y * (1 - c) + u.x * s;
	mat[10] = c + u.z * u.z * (1 - c);
}
