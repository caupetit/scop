/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_texture.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/28 19:34:16 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/03 16:58:30 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define GLFW_INCLUDE_GLCOREARB

#include "scop.h"
#include "error.h"
#include "stdlib.h"
#include "load_texture.h"

int				load_bmp(const char *nm, unsigned char **dt, unsigned int *inf)
{
	unsigned char	header[54];
	FILE			*file;

	if (!(file = fopen(nm, "rb")))
		return (p_error("opening texture"));
	if (fread(header, 1, 54, file) != 54)
		return (error("Not a correct BMP file"));
	if (header[0] != 'B' || header[1] != 'M')
		return (error("Not a correct BMP file"));
	inf[dataPos] = *(int*)&(header[0x0A]);
	inf[imgSize] = *(int*)&(header[0x22]);
	inf[width] = *(int*)&(header[0x12]);
	inf[height] = *(int*)&(header[0x16]);
	inf[imgSize] = inf[imgSize] == 0 ? width * height * 3 : inf[imgSize];
	inf[dataPos] = inf[dataPos] == 0 ? 54 : inf[dataPos];
	if (!(*dt = malloc(sizeof(unsigned char) * inf[imgSize])))
		return (p_error("Allocating texture data"));
	fread(*dt, sizeof(char), inf[imgSize], file);
	fclose(file);
	return (true);
}

int				load_texture(const char *name)
{
	unsigned char	*data;
	unsigned int	inf[4];
	GLuint			texture_id;

	if (!load_bmp(name, &data, inf))
		return (false);
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, inf[width], inf[height],
				0, GL_BGR, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
					GL_LINEAR_MIPMAP_LINEAR);
	free(data);
	return (texture_id);
}
