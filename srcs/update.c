/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/03 18:10:35 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/03 18:12:08 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <strings.h>
#include "scop.h"

void			update_mouse_axis(t_env *env)
{
	float		center;

	center = 0.04;
	if (env->mouse_axis[0] > center || env->mouse_axis[0] < -center)
		env->cam.yaw += env->mouse_axis[0];
	if (env->mouse_axis[1] > center || env->mouse_axis[1] < -center)
		env->cam.pitch += env->mouse_axis[1];
	update_cam_dir(&env->cam);
}

void			update_scene_center(t_env *env, int i)
{
	float		move;

	move = 0.2;
	if (i == k4)
		env->scene_center.x -= move;
	if (i == k6)
		env->scene_center.x += move;
	if (i == k7)
		env->scene_center.z -= move;
	if (i == k9)
		env->scene_center.z += move;
	if (i == k2)
		env->scene_center.y += move;
	if (i == k8)
		env->scene_center.y -= move;
}

void			update_inputs(t_env *env)
{
	int		i;

	i = 0;
	while (++i < inputs_nb)
	{
		if (env->inputs[i] && i >= up && i <= right)
			update_cam_angle(&env->cam, i);
		if (env->inputs[i] && i >= w && i <= d)
			update_cam_origin(&env->cam, i);
		if (env->inputs[i] && i >= one && i <= three)
			env->display_mode = i % one;
		if (env->inputs[i] && i >= k4 && i <= k9)
			update_scene_center(env, i);
		if (env->inputs[i] && i == space)
			bzero(env->mouse_axis, sizeof(double) * 2);
	}
	update_mouse_axis(env);
}
