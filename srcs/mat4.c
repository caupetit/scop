/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat4.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 13:56:37 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/21 16:17:07 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <strings.h>
#include "mat4.h"

void		ident_mat4(float *mat)
{
	bzero(mat, sizeof(float) * 16);
	mat[0] = 1;
	mat[5] = 1;
	mat[10] = 1;
	mat[15] = 1;
}

void		viewfps_mat4(float *mat, t_vec3 eye, float pitch, float yaw)
{
	float	coss[2];
	float	sins[2];
	t_vec3	axis[3];

	coss[0] = cos(pitch / 180.0 * M_PI);
	coss[1] = cos(yaw / 180.0 * M_PI);
	sins[0] = sin(pitch / 180.0 * M_PI);
	sins[1] = sin(yaw / 180.0 * M_PI);
	axis[0] = vec3(coss[1], 0, -sins[1]);
	axis[1] = vec3(sins[1] * sins[0], coss[0], coss[1] * sins[0]);
	axis[2] = vec3(sins[1] * coss[0], -sins[0], coss[0] * coss[1]);
	ident_mat4(mat);
	mat[0] = axis[0].x;
	mat[1] = axis[1].x;
	mat[2] = axis[2].x;
	mat[4] = axis[0].y;
	mat[5] = axis[1].y;
	mat[6] = axis[2].y;
	mat[8] = axis[0].z;
	mat[9] = axis[1].z;
	mat[10] = axis[2].z;
	mat[12] = -dotv3(axis[0], eye);
	mat[13] = -dotv3(axis[1], eye);
	mat[14] = -dotv3(axis[2], eye);
}

void		view_mat4(float *mat, t_vec3 eye, t_vec3 target, t_vec3 up)
{
	t_vec3	zaxis;
	t_vec3	xaxis;
	t_vec3	yaxis;

	zaxis = normalisev3(subv3(eye, target));
	xaxis = normalisev3(crossv3(up, zaxis));
	yaxis = crossv3(zaxis, xaxis);
	ident_mat4(mat);
	mat[0] = xaxis.x;
	mat[1] = yaxis.x;
	mat[2] = zaxis.x;
	mat[4] = xaxis.y;
	mat[5] = yaxis.y;
	mat[6] = zaxis.y;
	mat[8] = xaxis.z;
	mat[9] = yaxis.z;
	mat[10] = zaxis.z;
	mat[12] = -dotv3(xaxis, eye);
	mat[13] = -dotv3(yaxis, eye);
	mat[14] = -dotv3(zaxis, eye);
}

void		proj_mat4(float *mat, float fov, float far, float near)
{
	float	e;
	float	r;

	r = 1024.0 / 768;
	e = 1.0 / tan(fov / 2.0);
	ident_mat4(mat);
	mat[0] = 1.0 / r * e;
	mat[5] = e;
	mat[10] = -far / (far - near);
	mat[11] = -1;
	mat[14] = -far * near / (far - near);
	mat[15] = 0;
}
