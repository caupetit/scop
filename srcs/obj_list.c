/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj_list.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/01 19:08:56 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/03 15:33:25 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <strings.h>
#include "scop.h"
#include "error.h"

int				obj_push(t_3do_l **begin, t_3do *obj)
{
	t_3do_l	*tmp;
	t_3do_l	*new;
	t_3do	*elem;

	tmp = *begin;
	while (tmp && tmp->next)
		tmp = tmp->next;
	if (!(elem = malloc(sizeof(t_3do))))
		return (p_error(": allocating obj element"));
	memcpy(elem, obj, sizeof(*obj));
	if (!(new = malloc(sizeof(t_3do_l))))
		return (p_error(": allocating obj_l element"));
	bzero(new, sizeof(*new));
	new->elem = elem;
	if (!tmp)
		*begin = new;
	else
		tmp->next = new;
	return (true);
}

void			obj_list_del(t_3do_l **begin)
{
	if (*begin && (*begin)->next)
		obj_list_del(&(*begin)->next);
	if (!*begin)
		return ;
	free((*begin)->elem->vertex);
	free((*begin)->elem->indices);
	free((*begin)->elem->normals);
	free((*begin));
}
