/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 18:50:45 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/22 18:49:20 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <GLFW/glfw3.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "error.h"
#include "libft.h"

int					shader_error(GLuint shader, GLuint flag,
								int is_program, const char *info)
{
	GLint		ok;
	GLchar		error[4096];

	if (is_program)
		glGetProgramiv(shader, flag, &ok);
	else
		glGetShaderiv(shader, flag, &ok);
	if (ok == GL_FALSE)
	{
		if (is_program)
			glGetProgramInfoLog(shader, sizeof(error), NULL, error);
		else
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);
		printf("Shader error: %s: %s", info, error);
		return (1);
	}
	return (0);
}

static const char	*load_file(const char *name)
{
	FILE		*fp;
	char		*data;
	char		*line;
	size_t		len;
	size_t		shit;

	if (!(fp = fopen(name, "r")))
		return (NULL);
	fseek(fp, 0, SEEK_END);
	if (!(data = (char *)malloc(sizeof(char) * ftell(fp) + 1)))
		return (NULL);
	bzero(data, ftell(fp) + 1);
	fseek(fp, 0, SEEK_SET);
	len = 0;
	line = NULL;
	while (getline(&line, &shit, fp) != -1)
	{
		strncpy(&data[len], line, strlen(line));
		len += strlen(line);
	}
	ft_strdel(&line);
	fclose(fp);
	return (data);
}

static int			create_shader(GLuint shid, const char *name,
								const char *sufix)
{
	const char	*data;
	GLint		len;
	char		buf[1024];

	bzero(buf, 1024);
	strcpy(buf, name);
	strcpy(&buf[strlen(name)], sufix);
	if (!(data = load_file(buf)))
		return (p_error(buf));
	len = strlen(data);
	glShaderSource(shid, 1, &data, &len);
	glCompileShader(shid);
	free((void *)data);
	if (shader_error(shid, GL_COMPILE_STATUS, 0, buf))
		return (0);
	return (1);
}

GLuint				load_shader(char *shad)
{
	GLuint		vid;
	GLuint		fid;
	GLuint		shaderid;

	vid = glCreateShader(GL_VERTEX_SHADER);
	fid = glCreateShader(GL_FRAGMENT_SHADER);
	shaderid = glCreateProgram();
	if (!vid || !fid || !shaderid)
		return (error("Shaders creation"));
	if (!create_shader(vid, shad, ".v") || !create_shader(fid, shad, ".f"))
		return (0);
	glAttachShader(shaderid, vid);
	glAttachShader(shaderid, fid);
	glLinkProgram(shaderid);
	if (shader_error(shaderid, GL_LINK_STATUS, 1, "Program link"))
		return (0);
	glDeleteShader(vid);
	glDeleteShader(fid);
	return (shaderid);
}
