/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operators_mat4.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/13 17:50:13 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/21 16:18:27 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <strings.h>
#include "mat4.h"

float		*mult_mat4(float *r, float *a, float *b)
{
	int		i;
	int		j;
	float	res[16];

	bzero(res, sizeof(float) * 16);
	i = -1;
	while (++i < 16)
	{
		j = -1;
		while (++j < 4)
			res[i] += b[i / 4 * 4 + j] * a[i % 4 + j * 4];
	}
	i = -1;
	while (++i < 16)
		r[i] = res[i];
	return (r);
}
