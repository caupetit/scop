/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cam.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/21 15:56:40 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/01 21:47:54 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <strings.h>
#include "cam.h"
#include "scop.h"
#include "math.h"

void			update_cam_dir(t_cam *cam)
{
	float	pitch;
	float	yaw;

	pitch = cam->pitch * M_PI / 180.0;
	yaw = cam->yaw * M_PI / 180.0;
	cam->dir = vec3(-cos(pitch) * sin(yaw), sin(pitch), -cos(pitch) * cos(yaw));
	cam->dir = normalisev3(cam->dir);
	cam->up = normalisev3(vec3(sin(yaw), 1, cos(yaw)));
}

void			update_cam_angle(t_cam *cam, int input)
{
	if (input == up)
		cam->pitch += cam->rot_speed;
	if (input == down)
		cam->pitch -= cam->rot_speed;
	if (input == left)
		cam->yaw += cam->rot_speed;
	if (input == right)
		cam->yaw -= cam->rot_speed;
	update_cam_dir(cam);
}

void			update_cam_origin(t_cam *cam, int input)
{
	t_vec3	straf;

	if (input == w)
		cam->origin = addv3(cam->origin, cam->dir);
	if (input == s)
		cam->origin = subv3(cam->origin, cam->dir);
	cam->dir = normalisev3(cam->dir);
	if (cam->pitch >= -45)
		straf = crossv3(cam->dir, cam->up);
	else
		straf = crossv3(cam->up, cam->dir);
	straf = mult1v3(normalisev3(straf), 0.8);
	if (input == d)
		cam->origin = addv3(cam->origin, straf);
	if (input == a)
		cam->origin = subv3(cam->origin, straf);
}

void			init_cam(t_cam *cam)
{
	bzero(cam, sizeof(t_cam));
	cam->origin.z = 50;
	cam->rot_speed = 0.8;
}
