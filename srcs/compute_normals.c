/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_normals.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 16:56:04 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/04 20:09:01 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <strings.h>
#include <stdlib.h>
#include "scop.h"
#include "error.h"

int				calc_faces_normals(t_3do *obj)
{
	unsigned int	i;
	int				j;
	t_vec3			vtx[3];
	unsigned int	*po;

	if (!(obj->normals = malloc(sizeof(t_vec3) * obj->inb)))
		return (p_error("Allocating normals"));
	i = 0;
	while (i < obj->inb * 3)
	{
		po = &obj->indices[i];
		j = -1;
		while (++j < 3)
		{
			if (*(po + j) > obj->vnb)
				return (error("Invalid indice"));
			vtx[j] = vec3(obj->vertex[*(po + j) * 3],
						obj->vertex[*(po + j) * 3 + 1],
						obj->vertex[*(po + j) * 3 + 2]);
		}
		obj->normals[i / 3] = crossv3(subv3(vtx[0], vtx[1]),
									subv3(vtx[1], vtx[2]));
		i += 3;
	}
	return (true);
}

static void		calc_averages_normals(t_3do *obj, t_vec3 *vn)
{
	unsigned int	i;
	int				j;

	i = 0;
	while (i < obj->inb * 3)
	{
		j = -1;
		while (++j < 3)
		{
			vn[obj->indices[i + j]] = addv3(vn[obj->indices[i + j]],
											obj->normals[i / 3]);
		}
		i += 3;
	}
	j = -1;
	while (++j < (int)obj->vnb)
		vn[j] = normalisev3(vn[j]);
}

int				calc_vert_normals(t_3do *obj)
{
	t_vec3			*vn;

	if (!(vn = malloc(sizeof(t_vec3) * obj->vnb)))
		return (p_error("allocating vertix normals"));
	bzero(vn, sizeof(*vn) * obj->vnb);
	calc_averages_normals(obj, vn);
	free(obj->normals);
	obj->normals = vn;
	return (true);
}
