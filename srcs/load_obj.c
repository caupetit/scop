/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_obj.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/28 14:17:56 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/04 19:59:13 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define GLFW_INCLUDE_GLCOREARB

#include <math.h>
#include <strings.h>
#include <stdlib.h>
#include <dirent.h>
#include "scop.h"
#include "error.h"

static int		bind_object(t_3do *o, int *size)
{
	glGenVertexArrays(1, &(o->vao));
	glBindVertexArray(o->vao);
	glGenBuffers(1, &(o->vbo));
	glBindBuffer(GL_ARRAY_BUFFER, o->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * size[0],
				o->vertex, GL_STATIC_DRAW);
	glGenBuffers(1, &(o->vbon));
	glBindBuffer(GL_ARRAY_BUFFER, o->vbon);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * size[0],
				o->normals, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, o->vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, o->vbon);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glDisableVertexAttribArray(1);
	glEnableVertexAttribArray(0);
	glGenBuffers(1, &o->ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, o->ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size[1] * sizeof(unsigned int) * 3,
				o->indices, GL_STATIC_DRAW);
	glDisableVertexAttribArray(0);
	return (1);
}

static void		init_obj_values(t_3do *obj, int obj_size)
{
	float			mx[3];
	float			mn[4];
	unsigned int	i;
	int				j;

	bzero(mx, sizeof(float) * 3);
	bzero(mn, sizeof(float) * 4);
	i = 0;
	while (i < obj->vnb * 3)
	{
		j = -1;
		while (++j < 3)
		{
			mn[j] = obj->vertex[i + j] < mn[j] ? obj->vertex[i + j] : mn[j];
			mx[j] = obj->vertex[i + j] > mx[j] ? obj->vertex[i + j] : mx[j];
			obj->trans[j] += obj->vertex[i + j];
		}
		i += 3;
	}
	mn[3] = fabs(mx[1] - mn[1]);
	obj->scale = obj_size / mn[3];
	i = -1;
	while (++i < 3)
		obj->trans[i] = obj->trans[i] / obj->vnb * obj->scale;
}

void			set_objs_textures(t_env *env, t_3do_l **begin)
{
	t_3do_l		*tmp;
	static int	i = 0;

	tmp = *begin;
	while (tmp)
	{
		tmp->elem->texture_id = env->tid[i % 4];
		tmp = tmp->next;
		i++;
	}
}

int				load_obj(t_3do *obj, const char *name, GLuint shad)
{
	FILE		*fp;
	int			sizes[2];
	static char	msg[128];

	strcpy(msg, name);
	if (!(fp = fopen(name, "r")))
		return (p_error(strcat(msg, ": Opening file")));
	count_obj_data(fp, sizes);
	if (sizes[0] < 3 || !sizes[1])
		return (error(strcat(msg, ": Invalid .obj file")));
	bzero(obj, sizeof(*obj));
	obj->vnb = sizes[0];
	obj->inb = sizes[1];
	if (!(obj->vertex = malloc(sizes[0] * sizeof(GLfloat) * 3)))
		return (p_error(strcat(msg, ": Allocating vertices")));
	if (!(obj->indices = malloc(sizes[1] * sizeof(unsigned int) * 3)))
		return (p_error(strcat(msg, ": Allocating faces")));
	load_obj_data(fp, obj->vertex, obj->indices);
	if (!calc_faces_normals(obj) || !calc_vert_normals(obj))
		return (false);
	obj->shader = shad;
	init_obj_values(obj, OBJS_SIZE);
	bind_object(obj, sizes);
	return (true);
}

int				get_3do_in_dir(const char *name, t_3do_l **begin)
{
	DIR				*d;
	struct dirent	*dir;
	t_3do			obj;
	char			buf[512];
	GLuint			shader;

	if (!(d = opendir(name)))
		return (p_error(": openning objs directory"));
	shader = load_shader("assets/test");
	while ((dir = readdir(d)) != NULL)
	{
		if (strlen(dir->d_name) > 4 &&
			!strcmp(&dir->d_name[strlen(dir->d_name) - 4], ".obj"))
		{
			bzero(buf, 512);
			strcpy(buf, name);
			strcat(buf, dir->d_name);
			if (load_obj(&obj, buf, shader) && obj_push(begin, &obj))
				printf("Loaded %s\n", buf);
		}
	}
	closedir(d);
	if (!*begin)
		return (error("No Object loaded"));
	return (true);
}
