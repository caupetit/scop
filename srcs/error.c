/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/11 14:07:02 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/11 16:17:45 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		error(const char *what)
{
	printf("Error: %s\n", what);
	return (0);
}

int		p_error(const char *where)
{
	perror(where);
	return (0);
}
