/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj_pars.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/22 18:45:40 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/04 20:09:17 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <GLFW/glfw3.h>

static void		count_faces(char *line, int *ct)
{
	int			ret;
	float		x;

	if (line[0] == 'f' && ft_strcountchar(line, '/') > 3)
		ret = sscanf(line, "f %f/%f/%f %f/%f/%f %f/%f/%f %f/%f/%f\n",
					&x, &x, &x, &x, &x, &x, &x, &x, &x, &x, &x, &x);
	else if (line[0] == 'f' && ft_strcountchar(line, '/') == 3)
		ret = sscanf(line, "f %f/%f %f/%f %f/%f %f/%f\n",
					&x, &x, &x, &x, &x, &x, &x, &x);
	else if (line[0] == 'f')
		ret = sscanf(line, "f %f %f %f %f\n", &x, &x, &x, &x);
	else
		ret = 0;
	if (ret == 3 || ret == 6 || ret == 9)
		*ct += 1;
	else if (ret == 4 || ret == 8 || ret == 12)
		*ct += 2;
}

int				count_obj_data(FILE *fp, int *counts)
{
	char		*line;
	size_t		shit;

	line = NULL;
	bzero(counts, sizeof(int) * 2);
	while (getline(&line, &shit, fp) != -1)
	{
		if (strlen(line) < 2 || !(line[0] == 'v' || line[0] == 'f'))
			continue;
		if (line[0] == 'v' && line[1] == ' ')
			counts[0]++;
		else if (line[0] == 'f')
			count_faces(line, &counts[1]);
	}
	fseek(fp, 0, SEEK_SET);
	ft_strdel(&line);
	return (1);
}

static void		fill_indices(unsigned int *ind, char *line, int *ct)
{
	float		n[5];
	int			ret;

	if (ft_strcountchar(line, '/') > 3)
		ret = sscanf(line, "f %f/%f/%f %f/%f/%f %f/%f/%f %f/%f/%f\n",
					&n[0], &n[4], &n[4], &n[1], &n[4], &n[4],
					&n[2], &n[4], &n[4], &n[3], &n[4], &n[4]);
	else if (ft_strcountchar(line, '/') == 3)
		ret = sscanf(line, "f %f/%f %f/%f %f/%f %f/%f\n", &n[0], &n[4],
					&n[1], &n[4], &n[2], &n[4], &n[3], &n[4]);
	else
		ret = sscanf(line, "f %f %f %f %f\n", &n[0], &n[1], &n[2], &n[3]);
	if (ret != 4 && ret != 8 && ret != 12 && ret != 3 && ret != 6 && ret != 9)
		return ;
	ind[*ct] = n[0] - 1;
	ind[*ct + 1] = n[1] - 1;
	ind[*ct + 2] = n[2] - 1;
	*ct += 3;
	if (ret == 4)
	{
		ind[*ct] = n[2] - 1;
		ind[*ct + 1] = n[3] - 1;
		ind[*ct + 2] = n[0] - 1;
		*ct += 3;
	}
}

void			load_obj_data(FILE *fp, GLfloat *vertex, unsigned int *indices)
{
	float		num[5];
	int			ct[3];
	char		*line;
	size_t		shit;

	ct[0] = 0;
	ct[1] = 0;
	line = NULL;
	while (getline(&line, &shit, fp) != -1)
	{
		if (strlen(line) < 3 || !(line[0] == 'v' || line[0] == 'f'))
			continue;
		if (line[0] == 'v' && line[1] == ' ')
		{
			sscanf(line, "v %f %f %f\n",
				&num[0], &num[1], &num[2]);
			vertex[ct[0]] = num[0];
			vertex[ct[0] + 1] = num[1];
			vertex[ct[0] + 2] = num[2];
			ct[0] += 3;
		}
		else if (line[0] == 'f')
			fill_indices(indices, line, &ct[1]);
	}
	ft_strdel(&line);
}
