/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   callback.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/21 16:04:21 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/05 15:39:13 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "scop.h"
#include "libft.h"

void		error_callback(int error, const char *description)
{
	(void)error;
	ft_putendl_fd(description, (int)stderr);
}

void		handle_keys(GLFWwindow *wn, t_env *env)
{
	int			i;
	static int	inputs[inputs_nb][2] = {

	{escape, GLFW_KEY_ESCAPE},
	{w, GLFW_KEY_W}, {s, GLFW_KEY_S}, {a, GLFW_KEY_A}, {d, GLFW_KEY_D},
	{up, GLFW_KEY_UP}, {down, GLFW_KEY_DOWN}, {left, GLFW_KEY_LEFT},
	{right, GLFW_KEY_RIGHT},
	{one, GLFW_KEY_1}, {two, GLFW_KEY_2}, {three, GLFW_KEY_3},
	{k4, GLFW_KEY_KP_4}, {k6, GLFW_KEY_KP_6}, {k8, GLFW_KEY_KP_8},
	{k2, GLFW_KEY_KP_5}, {k7, GLFW_KEY_KP_7}, {k9, GLFW_KEY_KP_9},
	{space, GLFW_KEY_SPACE}};
	i = 0;
	while (++i < inputs_nb)
	{
		if (glfwGetKey(wn, inputs[i][1]) == GLFW_PRESS)
			env->inputs[i] = true;
		else
			env->inputs[i] = false;
	}
	if (glfwGetKey(wn, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(wn, GL_TRUE);
}

void		cursor_pos_callback(GLFWwindow *win, double xpos, double ypos)
{
	double			lastx;
	double			lasty;
	t_env			*env;
	int				width;
	int				height;

	glfwGetWindowSize(win, &width, &height);
	lastx = width / 2.0;
	lasty = height / 2.0;
	env = glfwGetWindowUserPointer(win);
	if (!env)
		return ;
	env->mouse_axis[0] = (lastx - xpos) / width;
	env->mouse_axis[1] = (lasty - ypos) / height;
}
