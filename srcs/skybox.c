/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   skybox.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/03 15:08:59 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/04 18:35:51 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define GLFW_INCLUDE_GLCOREARB

#include <GLFW/glfw3.h>
#include <stdlib.h>
#include "scop.h"
#include "mat4.h"
#include "load_texture.h"

static GLfloat g_sky_v[] = {
	-1000.0f, 1000.0f, -1000.0f,
	-1000.0f, -1000.0f, -1000.0f,
	1000.0f, -1000.0f, -1000.0f,
	1000.0f, -1000.0f, -1000.0f,
	1000.0f, 1000.0f, -1000.0f,
	-1000.0f, 1000.0f, -1000.0f,
	-1000.0f, -1000.0f, 1000.0f,
	-1000.0f, -1000.0f, -1000.0f,
	-1000.0f, 1000.0f, -1000.0f,
	-1000.0f, 1000.0f, -1000.0f,
	-1000.0f, 1000.0f, 1000.0f,
	-1000.0f, -1000.0f, 1000.0f,
	1000.0f, -1000.0f, -1000.0f,
	1000.0f, -1000.0f, 1000.0f,
	1000.0f, 1000.0f, 1000.0f,
	1000.0f, 1000.0f, 1000.0f,
	1000.0f, 1000.0f, -1000.0f,
	1000.0f, -1000.0f, -1000.0f,
	-1000.0f, -1000.0f, 1000.0f,
	-1000.0f, 1000.0f, 1000.0f,
	1000.0f, 1000.0f, 1000.0f,
	1000.0f, 1000.0f, 1000.0f,
	1000.0f, -1000.0f, 1000.0f,
	-1000.0f, -1000.0f, 1000.0f,
	-1000.0f, 1000.0f, -1000.0f,
	1000.0f, 1000.0f, -1000.0f,
	1000.0f, 1000.0f, 1000.0f,
	1000.0f, 1000.0f, 1000.0f,
	-1000.0f, 1000.0f, 1000.0f,
	-1000.0f, 1000.0f, -1000.0f,
	-1000.0f, -1000.0f, -1000.0f,
	-1000.0f, -1000.0f, 1000.0f,
	1000.0f, -1000.0f, -1000.0f,
	1000.0f, -1000.0f, -1000.0f,
	-1000.0f, -1000.0f, 1000.0f,
	1000.0f, -1000.0f, 1000.0f
};

static void	set_texture_filters(void)
{
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

static int	load_cubemap(void)
{
	GLuint				texture_id;
	unsigned char		*data;
	unsigned int		inf[4];
	int					i;
	static const char	*files[6] = {

	"assets/right.bmp", "assets/left.bmp",
	"assets/bottom.bmp", "assets/top.bmp",
	"assets/back.bmp", "assets/front.bmp"};
	glGenTextures(1, &texture_id);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture_id);
	i = -1;
	while (++i < 6)
	{
		data = NULL;
		if (!load_bmp(files[i], &data, inf))
			return (false);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, inf[width],
					inf[height], 0, GL_BGR, GL_UNSIGNED_BYTE, data);
		free(data);
	}
	set_texture_filters();
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	return (texture_id);
}

int			load_skybox(t_skybox *sky)
{
	sky->shader = load_shader("assets/skybox");
	if (!(sky->texture_id = load_cubemap()))
		return (0);
	glGenVertexArrays(1, &sky->vao);
	glBindVertexArray(sky->vao);
	glGenBuffers(1, &sky->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, sky->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_sky_v), g_sky_v, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	return (true);
}

void		display_sky(t_env *env, t_skybox *sky)
{
	GLfloat	proj[16];
	GLfloat	view[16];
	GLfloat	trans[16];
	GLuint	ids[3];

	proj_mat4(proj, 0.8, 1000000, 0.1);
	viewfps_mat4(view, env->cam.origin, env->cam.pitch, env->cam.yaw);
	trans_mat4(trans, env->cam.origin);
	glDepthMask(GL_FALSE);
	glBindVertexArray(sky->vao);
	glUseProgram(sky->shader);
	ids[0] = glGetUniformLocation(sky->shader, "view");
	glUniformMatrix4fv(ids[0], 1, GL_FALSE, view);
	ids[1] = glGetUniformLocation(sky->shader, "projection");
	glUniformMatrix4fv(ids[1], 1, GL_FALSE, proj);
	ids[2] = glGetUniformLocation(sky->shader, "model");
	glUniformMatrix4fv(ids[2], 1, GL_FALSE, trans);
	glBindTexture(GL_TEXTURE_CUBE_MAP, sky->texture_id);
	glUniform1i(glGetUniformLocation(sky->shader, "skybox"), 0);
	glEnableVertexAttribArray(0);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	glDisableVertexAttribArray(0);
	glDepthMask(GL_TRUE);
}
