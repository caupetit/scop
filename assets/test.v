#version 330 core

uniform mat4					view;
uniform mat4					proj;
uniform mat4					model;
layout(location = 0) in vec3	vpos;
layout(location = 1) in vec3	norms;
flat out vec3					position;
out vec3						realPos;
out vec3						camPos;
out vec3						nrms;
out vec3						psition;
out vec3						nrm;

void main()
{
	nrms = norms;
	psition = vpos;

	nrm = (model * vec4(norms, 0)).xyz;
	realPos = (view * model * vec4(vpos, 1.0)).xyz;

	position = vpos;
	gl_Position = proj * view * model * vec4(vpos, 1);
	camPos = normalize(
		(inverse(view) * vec4(0., 0., 0., 1.0) - model * vec4(vpos, 1.0)).xyz);
}
