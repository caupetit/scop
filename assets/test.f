#version 330 core

flat in	vec3		position;
out vec3			colorOut;
uniform sampler2D	tm;
uniform float		mode;
uniform float		mix_value;
in vec3				nrms;
in vec3				psition;
in vec3				nrm;
in vec3				realPos;
in vec3				camPos;

void main()
{
	vec3	colorTex;
	vec3	colorFaces;

	// faces:
	float	pos = position.x + position.y + position.z;
	pos = pos * pos + pos * pos;
	float	val = 0.05 + 0.2 * abs(cos(pos) + sin(pos));
	colorFaces = vec3(val, val, val);

	// texture:
	vec2 pp;
/*
	if (abs(nrms.x) < 0.576 && abs(nrms.y) < 0.576)
		pp = vec2(psition.x, psition.y);
	else if (abs(nrms.x) < 0.576 && abs(nrms.z) < 0.576)
		pp = vec2(psition.x, psition.z);
	else
		pp = vec2(psition.z, psition.y);
*/
	if (abs(dot(nrms, vec3(0, 1, 0))) > 0.58)
		pp = vec2(psition.z, psition.x);
	else if (abs(dot(nrms, vec3(1, 0, 0))) > 0.58)
		pp = vec2(psition.z, psition.y);
	else
		pp = vec2(psition.x, psition.y);
	if (mode > 1)
		colorTex = vec3(texture(tm, vec2(position.x, position.y)).rgb);
	else
		colorTex = vec3(texture(tm, vec2(pp.x, pp.y)).rgb);

	// lights:
	vec3	lightColor = vec3(.8, .8, 0.9);
	vec3	lightPos = vec3(-800, 1200, -900);
	vec3	posToLight = normalize(lightPos - realPos);
	float	diff = clamp(max(dot(normalize(nrm), posToLight), 0.0), 0.0, 1.0);

	// specular:
	float	angle = dot(-realPos, nrm);
	float	spec;
	if (dot(normalize(nrm), posToLight) < 0)
		spec = 0;
	else
		spec = .6 * pow(max(dot(reflect(posToLight, normalize(nrm)), -camPos), 0.0), 10);

	float light = 0.3 + diff + spec;

	colorOut = mix(colorFaces, colorTex, mix_value) * lightColor *1.1 * light;
}
