/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat4.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 13:54:39 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/21 16:17:15 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAT4_H
# define MAT4_H

# include <math.h>
# include "vec3.h"

void		ident_mat4(float *mat);
void		view_mat4(float *mat, t_vec3 eye, t_vec3 target, t_vec3 up);
void		viewfps_mat4(float *mat, t_vec3 eye, float pitch, float yaw);
void		viewfps_mat42(float *mat, t_vec3 eye, float pitch, float yaw);
void		proj_mat4(float *mat, float fov, float far, float near);

void		trans_mat4(float *mat, t_vec3 t);
void		scale_mat4(float *mat, t_vec3 v);
void		rot_mat4(float *mat, float angle, t_vec3 u);

float		*mult_mat4(float *r, float *a, float *b);

#endif
