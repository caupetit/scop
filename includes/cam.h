/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cam.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/21 15:58:39 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/21 16:38:03 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CAM_H
# define CAM_H

# include "vec3.h"

typedef struct	s_cam
{
	t_vec3	origin;
	t_vec3	dir;
	t_vec3	up;
	float	pitch;
	float	yaw;
	float	rot_speed;
}				t_cam;

void			update_cam_dir(t_cam *cam);
void			update_cam_origin(t_cam *cam, int input);
void			update_cam_angle(t_cam *cam, int input);
void			init_cam(t_cam *cam);

#endif
