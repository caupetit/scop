/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 16:15:58 by caupetit          #+#    #+#             */
/*   Updated: 2015/06/05 15:38:09 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOP_H
# define SCOP_H

# include <GLFW/glfw3.h>
# include <stdio.h>
# include "cam.h"

# define OBJS_SIZE	10
# define WIDTH		1280
# define HEIGHT		960

enum			e_inputs
{
	escape = 0,
	w, s, a, d,
	up, down, left, right,
	one, two, three,
	k4, k6, k8, k2, k7, k9,
	space,
	inputs_nb
};

enum			e_bool
{
	false = 0,
	true
};

typedef struct	s_3do
{
	GLuint			vbo;
	GLuint			vao;
	GLuint			vbon;
	GLuint			ibo;
	GLuint			shader;
	GLuint			texture_id;
	GLfloat			*vertex;
	unsigned int	*indices;
	unsigned int	vnb;
	unsigned int	inb;
	float			scale;
	float			trans[3];
	t_vec3			*normals;
}				t_3do;

typedef	struct	s_3do_l
{
	t_3do			*elem;
	struct s_3do_l	*next;
}				t_3do_l;

typedef struct	s_skybox
{
	GLuint		vao;
	GLuint		vbo;
	GLuint		texture_id;
	GLuint		shader;
}				t_skybox;

typedef struct	s_env
{
	int		inputs[inputs_nb];
	double	mouse_axis[2];
	double	display_mode;
	t_cam	cam;
	GLuint	tid[4];
	t_vec3	scene_center;
}				t_env;

/*
**		shader.c
*/
GLuint			load_shader(char *shad);
int				shader_error(GLuint shader, GLuint flag,
							int is_program, const char *info);

/*
**		callback.c
*/
void			handle_keys(GLFWwindow *wn, t_env *env);
void			error_callback(int error, const char *description);
void			cursor_pos_callback(GLFWwindow *win, double xpos, double ypos);

/*
**		pars_obj.c
*/
void			load_obj_data(FILE *fp, GLfloat *vertex, unsigned int *indices);
int				count_obj_data(FILE *fp, int *counts);

/*
**		load_obj.c (3satics)
*/
void			set_objs_textures(t_env *env, t_3do_l **begin);
int				load_obj(t_3do *obj, const char *name, GLuint shad);
int				get_3do_in_dir(const char *name, t_3do_l **begin);

/*
**		compute_normals.c
*/
int				calc_faces_normals(t_3do *obj);
int				calc_vert_normals(t_3do *obj);

/*
**		3do_list.c
*/
int				obj_push(t_3do_l **begin, t_3do *obj);
void			obj_list_del(t_3do_l **begin);

/*
**		display.c
*/
void			draw_obj(t_env *env, t_3do *obj, const t_vec3 pos);
void			display_objs(t_env *env, t_3do_l **begin);

/*
**		skybox.c (2 statics)
*/
int				load_skybox(t_skybox *sky);
void			display_sky(t_env *env, t_skybox *sky);

/*
**		update.c
*/
void			update_mouse_axis(t_env *env);
void			update_scene_center(t_env *env, int i);
void			update_inputs(t_env *env);

#endif
