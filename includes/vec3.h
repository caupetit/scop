/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 14:29:15 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/19 20:37:33 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VEC3_H
# define VEC3_H

typedef struct	s_vec3
{
	float	x;
	float	y;
	float	z;
}				t_vec3;

t_vec3			vec3(float x, float y, float z);
t_vec3			normalisev3(t_vec3 v);
float			dotv3(t_vec3 a, t_vec3 b);
t_vec3			crossv3(t_vec3 a, t_vec3 b);

t_vec3			subv3(t_vec3 a, t_vec3 b);
t_vec3			sub1v3(t_vec3 a, float n);

t_vec3			addv3(t_vec3 a, t_vec3 b);
t_vec3			add1v3(t_vec3 a, float n);

t_vec3			multv3(t_vec3 a, t_vec3 b);
t_vec3			mult1v3(t_vec3 a, float n);

#endif
