/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_texture.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/28 19:35:45 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/28 19:38:44 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LOAD_TEXTURE_H
# define LOAD_TEXTURE_H

enum	e_bmp
{
	dataPos,
	imgSize,
	width,
	height
};

int		load_bmp(const char *nm, unsigned char **data, unsigned int *infos);
int		load_texture(const char *name);

#endif
