# SCOP #

### OPENGL 4.1 .obj file displayer ###

Simple program displaying 3D .obj i a rotating circle with cat textures, ligth and skybox.


### Setup ###

* need cmake to be installed.
* on osx and linux
```
make
```
### Pictures ###

![scop1.jpg](https://bitbucket.org/repo/yX5rRb/images/4015617136-scop1.jpg)

![scop2.jpg](https://bitbucket.org/repo/yX5rRb/images/673423158-scop2.jpg)