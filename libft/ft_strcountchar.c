/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcountchar.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/22 18:37:24 by caupetit          #+#    #+#             */
/*   Updated: 2015/05/22 18:39:15 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_strcountchar(const char *line, const int ch)
{
	int		ret;

	ret = 0;
	while (*line)
	{
		if (*line == ch)
			ret++;
		line++;
	}
	return (ret);
}
